<!DOCTYPE html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ورود</title>
    <link rel="stylesheet" href="loginStyle.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
</head>
<body>
    <section id="login">

        <div class="login col-sm-9 col-md-3 mx-auto text-center">
            {{-- @include("errors.massage") --}}
            <div class="header mb-3">
                <h2> تایید کد  </h2>
                <hr>
            </div>
            <div class="main">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                      <div class="form-group">
                        <input type="text" name="number" class="form-control"  placeholder=" کد ارسال شده .." required>

                    <div class="text-center"><button type="submit">تایید</button></div>
                  </form>
            </div>
            <div class="footer">

            </div>
        </div>
    </section>

    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
</body>
<script>

  if(document.getElementById("alert")){
  let al = document.getElementById("alert");
  function myGreeting()
  {
    al.style.display = "none";
  }
const myTimeout = setTimeout(myGreeting, 6000);

  }
</script>
</html>
